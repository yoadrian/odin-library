/**
 * Pre-populated array with objects
*/
const myLibrary = [
    new Book('The Magus', 'John Fowles', 656, true),
    new Book('Eloquent JavaScript: A Modern Introduction to Programming', 'Marijn Haverbeke', 472, false),
    new Book('99 Bottles of OOP', 'Sandi Metz', 1, false),
    new Book("L'insoutenable légèreté de l'être", 'Milan Kundera', 476, true),
    new Book("99 Francs", "Frédéric Beigbeder", 282, false),
    new Book('A Smarter Way to Learn JavaScript: The New Tech-Assisted Approach That Requires Half the Effort', 'Mark Myers', 254, false),
    new Book('JavaScript: The Comprehensive Guide to Learning Professional JavaScript Programming', 'Philip Ackermann', 982, false),
    new Book("You Don't Know JS: ES6 & Beyond ", 'Kyle Simpson', 261, false),
];

function Book(title, author, pages, read) {
  this.title = title;
  this.author = author;
  this.pages = pages;
  this.read = read;
}

/**
 * opens the modal when the Add Book button is clicked
*/
function openModal() {
    const newBookButton = document.querySelector('[data-add-book-main]');
    newBookButton.addEventListener('click', (e) => {
    // show the modal
    const modal = document.querySelector('[data-modal]');
    modal.showModal();
    // reset the form
    const form = document.querySelector('[data-add-edit-book-form]');
    form.reset();
    // set the appropriate header
    const cardHeader = document.querySelector('[data-card-header]');
    cardHeader.innerText = 'Add a Book';
    // remove the Edit button from sight
    const editButton = document.querySelector('[data-book-edit]');
    editButton.classList.add('hidden');
    const addToLibraryButton = document.querySelector('[data-book-submit]');
    addToLibraryButton.classList.remove('hidden');
})
}
openModal();

function submitBookInfo() {
    const formSubmitButton = document.querySelector('[data-add-edit-book-form]');
    formSubmitButton.addEventListener('submit', (e) => {
        e.preventDefault();
        const modal = document.querySelector('[data-modal]');
        const form = document.querySelector('[data-add-edit-book-form]');
        const bookTitle = modal.querySelector('[data-book-title]').value;
        const bookAuthor = modal.querySelector('[data-book-author]').value;
        const bookPages = modal.querySelector('[data-book-pages]').value;
        let bookRead = modal.querySelector('[data-book-read]').checked;
        addBookToLibrary(bookTitle, bookAuthor, bookPages, bookRead);
        displayBooks();
        form.reset();
        modal.close();
    })
}
submitBookInfo();

function addBookToLibrary(title, author, pages, read) {
  const newBook = new Book(title, author, pages, read);
  myLibrary.unshift(newBook);
}

function displayBooks() {
    clearDisplay();
    for (let i = 0; i < myLibrary.length; i++) {
        const book = myLibrary[i];
        // Create a card display element for the book
        const bookCard = createBookCard(book)
        // Append the display element to the page
        appendToPage(bookCard);
    }
}

function clearDisplay() {
    const libraryContainer = document.querySelector('[data-library-container]');
    while (libraryContainer.hasChildNodes()) {
        libraryContainer.removeChild(libraryContainer.lastChild);
    }
}

function createBookCard(book) {
    // Create a DOM element, the book card
    const card = document.createElement('div');
    card.classList.add('book-card');
    const cardTitle = document.createElement('h2');
    cardTitle.classList.add('card-title');
    const cardAuthor = document.createElement('div');
    cardAuthor.classList.add('card-author');
    const cardPages = document.createElement('div');
    cardPages.classList.add('card-pages');
    const cardRead = document.createElement('div');
    const cardButtonContainer = document.createElement('div');
    cardButtonContainer.classList.add('card-button-container')
    const cardReadContainer = document.createElement('button');
    const cardEditContainer = document.createElement('button');
    const cardDeleteContainer = document.createElement('button');
    // Populate the element with book information
    cardTitle.innerText = `${book.title}`;
    cardAuthor.innerText = `Author: ${book.author}`;
    cardPages.innerText = `Pages: ${book.pages}`;
    if (book.read) {
        cardRead.innerText = 'Read: Yes';
    } else {
        cardRead.innerText = 'Read: No';
    }
    // Add data-attribute to the Delete button
    cardDeleteContainer.setAttribute('data-index', myLibrary.indexOf(book));
    cardDeleteContainer.classList.add('delete-book-button');
    // Add data-attribute to the Toggle Read button
    cardReadContainer.setAttribute('data-index', myLibrary.indexOf(book));
    cardReadContainer.classList.add('toggle-read-button');
    // Add data-attribute to the Edit button
    cardEditContainer.setAttribute('data-index', myLibrary.indexOf(book));
    cardEditContainer.classList.add('edit-book-button');
    // Add buttons to remove the book, edit it, and toggle its read status
    renderReadIcon(cardReadContainer);
    renderEditIcon(cardEditContainer);
    renderDeleteIcon(cardDeleteContainer);
    // append all elements to the card container
    cardButtonContainer.appendChild(cardReadContainer);
    cardButtonContainer.appendChild(cardEditContainer);
    cardButtonContainer.appendChild(cardDeleteContainer);
    card.appendChild(cardTitle);
    card.appendChild(cardAuthor);
    card.appendChild(cardPages);
    card.appendChild(cardRead);
    card.appendChild(cardButtonContainer);
    // Return the created element
    return card;
}

function appendToPage(item) {
    const libraryContainer = document.querySelector('[data-library-container]');
    libraryContainer.appendChild(item);
}

function renderReadIcon(node) {
    const fragment = new DocumentFragment();
    // creating the svg element
    const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const iconPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    // setting svg attributes
    iconSvg.setAttribute('fill', 'black');
    iconSvg.setAttribute('viewBox', '0 0 24 24');
    iconPath.setAttribute('d', 'M16.75 22.16L14 19.16L15.16 18L16.75 19.59L20.34 16L21.5 17.41L16.75 22.16M6 22C4.89 22 4 21.1 4 20V4C4 2.89 4.89 2 6 2H7V9L9.5 7.5L12 9V2H18C19.1 2 20 2.89 20 4V13.34C19.37 13.12 18.7 13 18 13C14.69 13 12 15.69 12 19C12 20.09 12.29 21.12 12.8 22H6Z');
    // creating the text node of the button
    const favoriteText = document.createElement('span');
    favoriteText.innerText = 'Read?';
    favoriteText.classList.add('toggle-read-button-text')
    // appending nodes
    iconSvg.appendChild(iconPath);
    fragment.appendChild(iconSvg);
    fragment.appendChild(favoriteText);
    return node.appendChild(fragment);
}

function renderEditIcon(node) {
    const fragment = new DocumentFragment();
    // creating the svg element
    const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const iconPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    // setting svg attributes
    iconSvg.setAttribute('fill', 'black');
    iconSvg.setAttribute('viewBox', '0 0 24 24');
    iconPath.setAttribute('d', 'M6 2C4.9 2 4 2.9 4 4V20C4 21.1 4.9 22 6 22H10V20.1L20 10.1V8L14 2H6M13 3.5L18.5 9H13V3.5M20.1 13C20 13 19.8 13.1 19.7 13.2L18.7 14.2L20.8 16.3L21.8 15.3C22 15.1 22 14.7 21.8 14.5L20.5 13.2C20.4 13.1 20.3 13 20.1 13M18.1 14.8L12 20.9V23H14.1L20.2 16.9L18.1 14.8Z');
    // creating the text node of the button
    const editText = document.createElement('span');
    editText.innerText = 'Edit';
    editText.classList.add('edit-button-text');
    // appending nodes
    iconSvg.appendChild(iconPath);
    fragment.appendChild(iconSvg);
    fragment.appendChild(editText);
    return node.appendChild(fragment);
}

function renderDeleteIcon(node) {
    const fragment = new DocumentFragment();
    // creating the svg element
    const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const iconPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    // setting svg attributes
    iconSvg.setAttribute('viewBox', '0 0 24 24');
    iconPath.setAttribute('d', 'M21.12 15.46L19 17.59L16.88 15.46L15.46 16.88L17.59 19L15.46 21.12L16.88 22.54L19 20.41L21.12 22.54L22.54 21.12L20.41 19L22.54 16.88M6 2C4.89 2 4 2.89 4 4V20C4 21.11 4.89 22 6 22H13.81C13.28 21.09 13 20.05 13 19C13 18.67 13.03 18.33 13.08 18H6V16H13.81C14.27 15.2 14.91 14.5 15.68 14H6V12H18V13.08C18.33 13.03 18.67 13 19 13C19.34 13 19.67 13.03 20 13.08V8L14 2M13 3.5L18.5 9H13Z');
    // creating the text node of the button
    const deleteText = document.createElement('span');
    deleteText.innerText = 'Delete';
    deleteText.classList.add('delete-button-text');
    // appending nodes
    iconSvg.appendChild(iconPath);
    fragment.appendChild(iconSvg);
    fragment.appendChild(deleteText);
    return node.appendChild(fragment);
}

function deleteBook() {
    document.querySelector('body').addEventListener('click', (event) => {
        if (event.target.classList.contains('delete-book-button')) {
            const bookIndex = event.target.dataset.index;
            myLibrary.splice(bookIndex, 1);
            displayBooks();
        }
    })
}
deleteBook();

/**
 * change the reading status add to the prototype of Book
*/
Book.prototype.toggleRead = function() {
    this.read = !this.read;
}

function changeReadStatus() {
    document.querySelector('body').addEventListener('click', (event) => {
        if (event.target.classList.contains('toggle-read-button')) {
            const bookIndex = event.target.dataset.index;
            myLibrary[bookIndex].toggleRead();
            displayBooks();
        }
    })
}
changeReadStatus();

/**
 * opens the modal and populates it the book info we want to edit
*/
function editBook() {
    document.querySelector('body').addEventListener('click', (event) => {
        if (event.target.classList.contains('edit-book-button')) {
            // select DOM elements
            const bookIndex = event.target.dataset.index;
            const cardHeader = document.querySelector('[data-card-header]');
            const editButton = document.querySelector('[data-book-edit]');
            const modal = document.querySelector('[data-modal]');
            const bookTitle = modal.querySelector('[data-book-title]');
            const bookAuthor = modal.querySelector('[data-book-author]');
            const bookPages = modal.querySelector('[data-book-pages]');
            let bookRead = modal.querySelector('[data-book-read]');
            // show modal
            modal.showModal();
            // populate DOM elements
            cardHeader.innerText = 'Edit Book';
            bookTitle.value = myLibrary[bookIndex].title;
            bookAuthor.value = myLibrary[bookIndex].author;
            bookPages.value = myLibrary[bookIndex].pages;
            bookRead.checked = myLibrary[bookIndex].read;
            // remove the Add to Library button from sight
            editButton.classList.remove('hidden');
            const addToLibraryButton = document.querySelector('[data-book-submit]');
            addToLibraryButton.classList.add('hidden');
            // attach index of book as data-attribute to the Edit button
            editButton.setAttribute('data-index', bookIndex);
        }
    })
}
editBook();

function submitEditedInfo() {
    const formSubmitEditButton = document.querySelector('[data-book-edit]');
    formSubmitEditButton.addEventListener('click', (e) => {
        // select index and elements
        const bookIndex = e.target.dataset.index;
        const modal = document.querySelector('[data-modal]');
        const form = document.querySelector('[data-add-edit-book-form]');
        const bookTitle = modal.querySelector('[data-book-title]').value;
        const bookAuthor = modal.querySelector('[data-book-author]').value;
        const bookPages = modal.querySelector('[data-book-pages]').value;
        let bookRead = modal.querySelector('[data-book-read]').checked;
        // create temp object with edited content
        const editedBookObject = new Book(bookTitle, bookAuthor, bookPages, bookRead);
        // replace array element with edited element
        myLibrary[bookIndex] = editedBookObject;
        // display the new array, reset, and close
        displayBooks();
        form.reset();
        modal.close();
    })
}
submitEditedInfo();

function closeModal() {
    const close = document.querySelector('[data-close-modal]');
    close.addEventListener('click', () =>{
        const modal = document.querySelector('[data-modal]');
        modal.close();
    })
}
closeModal();

displayBooks();