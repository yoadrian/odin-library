# Odin Library

Access the live page at: [https://yoadrian.gitlab.io/odin-library/](https://yoadrian.gitlab.io/odin-library/)

## Project learning-points
- the importance of having well written pseudocode before writing any line of code
- object constructors
- object prototype
- prototypal inheritance
- array methods
- usage of the `this` keyword